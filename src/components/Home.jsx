import React from "react";
import Connection from "./Connection";
import Alert from "react-bootstrap/Alert"
import Config from "../config/Config"
import Teleoperation from "./Teleoperation";
import { Row, Col, Container, Button } from "react-bootstrap"


class Home extends React.Component {

    state = { isConnected: false, ros: null, };

    constructor() {
        super();
        this.init_connection();
    }

    init_connection() {

        this.state.ros = new window.ROSLIB.Ros();
        console.log(this.state.ros);



        this.state.ros.on("connection", () => {
            console.log("connection established!");
            this.setState({ isConnected: true });
        })

        this.state.ros.on("close", () => {
            console.log("connection closed!");
            this.setState({ isConnected: false });
            // try to reconnect
            setTimeout(() => {
                try {
                    this.state.ros.connect("ws://" + Config.ROSBRIDGE_SERVER_IP + ":" + Config.ROSBRIDGE_SERVER_PORT);
                }
                catch (error) { console.log("Rosbridge reconnection problem.") }

            }, Config.RECONNECTION_TIMER_us);
        })

        try {
            this.state.ros.connect("ws://" + Config.ROSBRIDGE_SERVER_IP + ":" + Config.ROSBRIDGE_SERVER_PORT);
        }
        catch (error) { console.log("Rosbridge connection problem.") }


    }

    render() {
        return (<div>
            <Container>
                <h1 className="text-center mt-3"> Robot Control Page </h1>
                <Row>
                    <Col>
                        <Connection />
                        <Alert className="text-center"
                            variant={this.state.isConnected ? "success" : "danger"}>
                            {this.state.isConnected ? "Connected" : "Disconnected"}
                        </Alert>
                    </Col>
                </Row>

                <Row>
                    <Col>
                       
                        <Teleoperation />
                    </Col>
                    <Col>
                        <h1>MAP</h1>
                        <p>This region for map</p>
                    </Col>
                </Row>



            </Container>
        </div>)
    }
}

export default Home;